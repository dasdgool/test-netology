import { connect } from "react-redux";
import { checkUser } from "../actions";
import { StaffTableBody } from "../components/StaffTable/StaffTableBody/StaffTableBody";

const mapStateToProps = (state: any) => ({ rows: state.staff });

const mapDispatchToProps = (dispatch: any) => ({
  onCheckUser: (id: string) => dispatch(checkUser(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(StaffTableBody);
