import { connect } from "react-redux";
import { checkAll } from "../actions";
import { StaffTableHeader } from "../components/StaffTable/StaffTableHeader/StaffTableHeader";

const mapDispatchToProps = (dispatch: any) => ({
  onClick: () => dispatch(checkAll()),
});

export default connect(null, mapDispatchToProps)(StaffTableHeader);
