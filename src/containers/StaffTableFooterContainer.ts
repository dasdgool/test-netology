import { connect } from "react-redux";
import { StaffTableFooter } from "../components/StaffTable/StaffTableFooter/StaffTableFooter";

const mapStateToProps = (state: any) => ({ users: state.staff });

export default connect(mapStateToProps)(StaffTableFooter);
