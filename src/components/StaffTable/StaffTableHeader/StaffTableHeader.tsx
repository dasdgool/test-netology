import React from "react";
import "./style.css";
import { Checkbox, FormControlLabel } from "@material-ui/core";

interface StaffTableHeaderProps {
  onClick: () => void;
}

export const StaffTableHeader = ({ onClick }: StaffTableHeaderProps) => {
  return (
    <div className="header">
      <FormControlLabel
        control={<Checkbox className="header__checkbox" onChange={onClick} />}
        label="Выделить всё"
      />
    </div>
  );
};
