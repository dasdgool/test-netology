import { Table } from "@material-ui/core";
import React from "react";
import StaffTableBodyContainer from "../../containers/StaffTableBodyContainer";
import StaffTableHeaderContainer from "../../containers/StaffTableHeaderContainer";
import StaffTableFooterContainer from "../../containers/StaffTableFooterContainer";

export interface User {
  id: string;
  checked: boolean;
  name: string;
  lastName: string;
  age: number;
}

export const StaffTable = () => {
  return (
    <>
      <StaffTableHeaderContainer />
      <Table>
        <StaffTableBodyContainer />
      </Table>
      <StaffTableFooterContainer />
    </>
  );
};
