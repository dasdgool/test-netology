import React from "react";
import {
  Checkbox,
  TableBody as TableBod,
  TableCell,
  TableRow,
} from "@material-ui/core";
import { User } from "../StaffTable";

interface StaffTableBodyProps {
  rows: Array<User>;
  onCheckUser: (id: string) => void;
}

export const StaffTableBody = ({
  rows = [],
  onCheckUser,
}: StaffTableBodyProps) => {
  return (
    <TableBod>
      {rows.map(({ id, name, lastName, checked, age }) => (
        <TableRow key={id} selected={checked}>
          <TableCell padding="checkbox">
            <Checkbox
              onChange={() => onCheckUser(id)}
              checked={checked}
            ></Checkbox>
          </TableCell>
          <TableCell>{name}</TableCell>
          <TableCell>{lastName}</TableCell>
          <TableCell>{age}</TableCell>
        </TableRow>
      ))}
    </TableBod>
  );
};
