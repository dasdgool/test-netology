import React from "react";
import { StaffTableHeader } from "../StaffTableHeader/StaffTableHeader";
import { StaffTableFooter } from "../StaffTableFooter/StaffTableFooter";
import { StaffTableBody } from "../StaffTableBody/StaffTableBody";
import { getUser } from "../../../util/getUser";

export default {
  title: "StaffTable",
};

const users = new Array(5)
  .fill(5)
  .map(() => getUser())
  .map((user) => ({ ...user, checked: user.age > 50 }));

export const SimpleHeader = () => (
  <StaffTableHeader onClick={() => console.log("all checked")} />
);

export const SimpleFooter = () => <StaffTableFooter users={users} />;

export const SimpleBody = () => (
  <StaffTableBody
    rows={users}
    onCheckUser={(id) => console.log(`${id} checked`)}
  />
);
