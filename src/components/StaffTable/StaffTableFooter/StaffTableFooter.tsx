import React from "react";
import "./style.css";
import { User } from "../StaffTable";
interface StaffTableFooterProps {
  users: Array<User>;
}

export const StaffTableFooter = ({ users = [] }: StaffTableFooterProps) => {
  return (
    <div className="footer">
      Пользователи:{" "}
      {users
        .filter((user) => user.checked)
        .map((user, id, arr) => (
          <span key={`checked_${user.id}`}>
            {user.name}
            {id !== arr.length - 1 ? ", " : " "}
          </span>
        ))}
    </div>
  );
};
