import { getUser } from "../util/getUser";

export enum staffTableActionTypes {
  CHECK_ALL,
  CHECK_ID,
  REQUEST_USERS,
  RECEIVE_USERS,
}

export const checkAll = () => ({
  type: staffTableActionTypes.CHECK_ALL,
});

export const checkUser = (id: string) => ({
  type: staffTableActionTypes.CHECK_ID,
  id,
});

function requestUsers() {
  return {
    type: staffTableActionTypes.REQUEST_USERS,
  };
}

function receiveUsers(json: any) {
  return {
    type: staffTableActionTypes.RECEIVE_USERS,
    users: json.users,
  };
}

export function fetchUsers() {
  return function (dispatch: any) {
    dispatch(requestUsers());
    const newUsers = new Array(10).fill(0).map(() => getUser());
    return setTimeout(() => dispatch(receiveUsers({ users: newUsers })), 3000);
  };
}
