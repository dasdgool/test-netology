import { combineReducers } from "redux";
import staff from "./staffReducer";

export default combineReducers({
  staff,
});
