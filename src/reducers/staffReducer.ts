import { staffTableActionTypes } from "../actions";
import { User } from "../components/StaffTable/StaffTable";

const initialState = [
  {
    id: "1234",
    name: "Mikha",
    lastName: "Keselman",
    age: 24,
    checked: false,
  },
];

const staff = (state = initialState, action: any) => {
  switch (action.type) {
    case staffTableActionTypes.RECEIVE_USERS:
    return [...state, ...action.users];
    case staffTableActionTypes.CHECK_ID:
      return state.map((user: User) =>
        user.id === action.id ? { ...user, checked: !user.checked } : user
      );

    case staffTableActionTypes.CHECK_ALL:
      const checked = state.reduce((prev, cur) => prev && cur.checked, true);
      return state.map((user: User) => ({ ...user, checked: !checked }));
    default:
      return state;
  }
};

export default staff;
