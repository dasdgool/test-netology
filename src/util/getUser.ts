import { v4 } from "uuid";

const getAge = () => Math.round(Math.random() * 100);
const getName = () => {
  const names = [
    "Kesha",
    "Miha",
    "Lusy",
    "Olya",
    "Gor",
    "Ivan",
    "Zoya",
    "Koi",
    "Denis",
  ];
  return names[Math.round(Math.random() * (names.length - 1))];
};
const getLastName = () => {
  const lastNames = [
    "Koinov",
    "Keselman",
    "Zukerman",
    "Mendeleev",
    "Gogol",
    "Evgrafov",
    "Martinovich",
    "SuperLongFAmilyName",
    "Car",
  ];
  return lastNames[Math.round(Math.random() * (lastNames.length - 1))];
};
export const getUser = () => ({
  id: v4(),
  name: getName(),
  lastName: getLastName(),
  age: getAge(),
  checked: false,
});
