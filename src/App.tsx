import React, { useEffect } from "react";
import { connect } from "react-redux";

import "./App.css";
import { StaffTable } from "./components/StaffTable/StaffTable";
import { fetchUsers } from "./actions";

function App(props: any) {
  useEffect(() => {
    const { dispatch } = props;
    fetchUsers()(dispatch);
  });
  return (
    <>
      <h2>Тестовое задание</h2>
      <StaffTable />
    </>
  );
}

export default connect()(App);
